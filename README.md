# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Chat room]
* Key functions (add/delete)
    1. [chat]
    2. [load message history]
    3. [chat with new user]
    4. [xxx]
* Other functions (add/delete)
    1. [user page]
    2. [xxx]
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
gitlab: https://105062334.gitlab.io/Midterm_Project
firebase: https://midtermproject-14525.firebaseapp.com/

　　在signin中可以看到背景用了CSS Animation來做動態的換色。這是一個登入、註冊的介面，註冊的話直接輸入想要用的信箱和密碼並按「新建帳號」，經過檢查合法後就帳號可以使用囉；登入的話可以使用剛剛創建的帳號或是google帳號進行登入。若是要用創建的帳號登入，在信箱欄和密碼欄輸入後按「登入」即可；若是要用google帳號登入，按「用Google帳號登入」後，會跳出頁面，輸入帳密後就可以登入囉。

　　在index中就是我們的聊天室了，要看到內容必須先登入才行。上排Account點開可以看到我們的信箱、Profile鈕（按下後連結到個人資料頁）、Logout鈕。網頁中間就是我們的聊天室，可以看到誰說了什麼。網頁最下方就是我們可以用來留言的地方，在空白處打上想說的話，再按送出和人聊天吧！這個網頁是以Lab06_SimpleForum所修改而來的，所以除了既有的功能之外，增修的地方為CSS的排版設計以及一些功能。主要有下列功能：
1. 將每個留言以X樓表示：最近常常逛巴哈，發現這個功能非常有用，除了有多少個聊天一目了然之外，要回覆特定的人時，可以不用抄下落落長的帳號，直接以幾樓來表示喔。
2. 將名稱簡化：我們是以信箱註冊進這個網站，所以顯示帳號時會連"@"後都顯示出來，但這其實沒什麼太大意義，甚至還會占版面顯得礙眼，所以決定只把ID定為"@"前的名稱。
3. 加入留言時間：說到聊天室就不得不加入時間，不管是LINE還是Facebook，聊天時總會顯示這個送出時間，讓人知道是什麼時候回話的，非常重要啊。
4. 顯示通知：有時在做其他事，沒注意網頁動態又怕錯過回話時機怎麼辦？只要在首次開啟網站的時候在顯示通知按個允許，就可以在別人回話的同時收到通知，讓你不怕錯過喔。

　　在profile中可以看到用戶的基本資料，包括大頭貼、ID、稱謂、自我介紹等等，沒有登入就看不到，不過這些都是不能修改的......

　　以上將這次Midterm_Project的功能說明完畢，謝謝助教。
## Security Report (Optional)
