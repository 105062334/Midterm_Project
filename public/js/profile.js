function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='profile-btn'>Profile</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).catch(function(e)
                {
                  create_alert("error",e.message);
                });;
              })


              var str_after_content = "</p></div></div>\n";
              var my_profile = document.getElementById('post_list');
          
              var a;
              for(a=0;a<user.email.length;a++){
                  if(user.email[a] == '@') break;
              }
              var aa = user.email.substring(0,a);
              console.log(aa);
              var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>" + "ID: " + aa + "</h6><div class='media text-muted pt-3'><img src='img/test.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
          
                      
              
              my_profile.innerHTML = str_before_username + "新加入的成員" + "</strong>" + "安安~請多多指教" + str_after_content;


        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
};