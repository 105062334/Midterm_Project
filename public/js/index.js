function init() {
    if (Notification.permission === 'default' || Notification.permission === 'undefined') {
        Notification.requestPermission();
    }
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='profile-btn'>Profile</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).catch(function(e)
                {
                  create_alert("error",e.message);
                });;
              })

            var btnProfile = document.getElementById('profile-btn');
            btnProfile.addEventListener('click', function(e){
                window.location.href='https://midtermproject-14525.firebaseapp.com/profile.html';
            })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var nowDate = new Date();  

    var hour = (nowDate.getHours() >= 10) ? nowDate.getHours().toString() : '0'+nowDate.getHours().toString();
    var min = (nowDate.getMinutes() >= 10) ? nowDate.getMinutes().toString() : '0'+nowDate.getMinutes().toString();


    var _time = nowDate.getFullYear().toString() + "/" + (nowDate.getMonth()+1).toString() + "/" + nowDate.getDate().toString() + " " + hour + ":" + min;

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var Ref = firebase.database().ref('com_list');
            var data = {
                data: post_txt.value,
                email: user_email,
                time: _time
            };
            Ref.push(data);
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var i = 1;

            snapshot.forEach(function(childshot){
                var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>" + i.toString() + "樓</h6><div class='media text-muted pt-3'><img src='img/test.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                var data = childshot.val();
                
                
                var a;
                for(a=0;a<data.email.length;a++){
                    if(data.email[a] == '@') break;
                }
                aa = data.email.substring(0,a);
                
                total_post[total_post.length] = str_before_username + aa + "    at " + data.time + "</strong>" + data.data + str_after_content;
                first_count += 1;
                i +=1;
            });


            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>" + i.toString() + "樓</h6><div class='media text-muted pt-3'><img src='img/test.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                    var childData = data.val();

                    var b;
                    for(b=0;b<childData.email.length;b++){
                        if(childData.email[b] == '@') break;
                    }
                    bb = childData.email.substring(0,b);

                    total_post[total_post.length] = str_before_username + bb + "   at " + childData.time + "</strong>" + childData.data + str_after_content
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    if(Notification.permission === 'granted'){
                        var user = firebase.auth().currentUser;
                        if(childData.email!=user.email){
                            var j;
                            for(j=0;j<childData.email.length;j++){
                                if(childData.email[j] == '@') break;
                            }
                            email_name = childData.email.substring(0,j);
                            var string = email_name + "回覆啦！";
                            var notification = new Notification(string,{
                                body: childData.data,
                                icon: 'https://i.imgur.com/QfrHzKC.png'
                            });
                            notification.onclick = function(e) {
                                e.preventDefault();
                                notification.close();
                            }
                        }
                    }
                    i += 1;
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};